package models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Address {

    public Address() {}
    public Address(int zipcode, String city, String street) {
        this.zipcode = zipcode;
        this.city = city;
        this.street = street;
    }
    public int getId() { return id; }
    public int getZipcode() {
        return zipcode;
    }
    public String getCity() {
        return city;
    }
    public String getStreet() {
        return street;
    }
    public void setId(int id) { this.id = id; }
    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return getZipcode() + " " + getCity() + ", " +getStreet();
    }

    @Id
    private int id;

    private int zipcode;
    private String city;
    private String street;
}
