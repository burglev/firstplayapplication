package models;

import javax.persistence.*;

import io.ebean.*;
import play.data.validation.*;

@Entity
public class Person extends Model {

    public Person() {}
    public Person(String name, String date) {
        this.name = name;
        this.birthdate = date;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    @Id
    private int id;

    @Constraints.Required
    private String name;

    private String birthdate;
}
