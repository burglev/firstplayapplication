package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;

import static play.mvc.Results.ok;

public class RouterActor extends AbstractActor {
    public static Props getProps() {
        return Props.create(RouterActor.class);
    }

    @Override
    public Receive createReceive() {
        System.out.println("createReceive() started");
        return receiveBuilder()
                .match(PersonActorProtocol.DeletePersonMessage.class, message -> {
                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
                    sender().tell(ok("Person deleting started..."), self());
                })
                .match(PersonActorProtocol.PersonCreateMessage.class, message -> {
                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
                    sender().tell(ok("Person creating started..."), self());
                })
                .match(PersonActorProtocol.PersonModifyMessage.class, message -> {
                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
                    sender().tell(ok("Person modifying started..."), self());
                })

                /*
                 * A PersonDetail, a PersonList, és a PersonListRender azért van kikommentellve,
                 * mert ott fontosak a visszaérkező adatok (és a folyamatos TCP kapcsolat),
                 * és a RouterActor ezekkel nem törődik, csak elküldi a feladatokat a többi actornak, és ezután visszajelez,
                 * de a végrehajtás eredményét már nem várja meg.
                 *
                 * A detail és a list függvényeket a PersonController osztályban működnek.
                 * A listRender máshol...
                 * A FileUpload pedig a FileUploadController osztályban.
                 */
//                .match(PersonActorProtocol.PersonDetailMessage.class, message -> {
//                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
//                    sender().tell(ok("Start deleting..."), self());
//                })
//                .match(PersonActorProtocol.PersonListMessage.class, message -> {
//                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
//                    sender().tell(ok("Start deleting..."), self());
//                })
//                .match(PersonActorProtocol.PersonListRenderMessage.class, message -> {
//                    context().actorOf(Props.create(PersonActor.class)).tell(message, self());
//                    sender().tell(ok("Start deleting..."), self());
//                })
//                .match(FileActorProtocol.FileUploadMessage.class, message -> {
//                    context().actorOf(Props.create(FileActor.class)).tell(message, self());
//                    sender().tell(ok("File upload started..."), self());
//                })

                .build();
    }
}
