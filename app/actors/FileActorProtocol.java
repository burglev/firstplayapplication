package actors;

import play.mvc.Http;

public class FileActorProtocol {

    public static class FileUploadMessage {
        public FileUploadMessage(Http.RequestBody body, String fileFolder) {
            this.body = body;
            this.fileFolder = fileFolder;
        }
        public Http.RequestBody body;
        public String fileFolder;
    }
}
