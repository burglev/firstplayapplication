package actors;

import akka.actor.UntypedAbstractActor;

public class HelloLocalActor extends UntypedAbstractActor {

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof MyMessage) {
            MyMessage myMessage = (MyMessage) message;
            myMessage.setMessage("Local Hello, " + myMessage.getMessage());
            getSender().tell(myMessage, getSelf());
        } else {
            unhandled(message);
        }
    }
}
