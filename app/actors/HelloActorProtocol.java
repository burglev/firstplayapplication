package actors;

import com.fasterxml.jackson.databind.JsonNode;
import models.Person;

public class HelloActorProtocol {

    public static class SayHello {
        public final String name;
        public final long ms;

        public SayHello(String name, long ms) {
            this.name = name;
            this.ms = ms;
            System.out.println("SayHello!");
        }
    }

    public static class PersonList {
        public PersonList() {}

    }
}
