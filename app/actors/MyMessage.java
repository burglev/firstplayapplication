package actors;

import java.io.Serializable;

public class MyMessage implements Serializable {

    public MyMessage() {}
    public MyMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
