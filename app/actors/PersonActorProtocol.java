package actors;

import com.fasterxml.jackson.databind.JsonNode;

public class PersonActorProtocol {

    public static class PersonCreateMessage {
        public PersonCreateMessage(JsonNode body) {
            this.body = body;
        }
        public JsonNode body;
    }

    public static class PersonListMessage {

    }

    public static class PersonListRenderMessage {

    }

    public static class PersonDetailMessage {
        public PersonDetailMessage(Long id) {
            this.id = id;
        }
        public Long id;
    }

    public static class DeletePersonMessage {
        public DeletePersonMessage(Long id) {
            this.id = id;
            System.out.println("nabammeg MSG");
        }
        public Long id;
    }

    public static class PersonModifyMessage {
        public PersonModifyMessage(Long id, JsonNode body) {
            this.id = id;
            this.body = body;
        }
        public Long id;
        public JsonNode body;
    }
}
