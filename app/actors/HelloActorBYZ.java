package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;

import static play.mvc.Results.ok;

public class HelloActorBYZ extends AbstractActor {

    public static Props getProps() {
        return Props.create(HelloActorBYZ.class);
    }

    @Override
    public Receive createReceive() {
        System.out.println("HelloActorBYZ::createReceive()");
        return receiveBuilder()
                .match(HelloActorProtocol.SayHello.class, hello -> {
                    System.out.println("HelloActorBYZ::createReceive() in match");
                    String reply = "Hello, " + hello.name + " - " + hello.ms;

                    Thread.sleep(hello.ms);

                    context().stop(self());
                    sender().tell(ok(reply), self());
                })
                .build();
    }
}
