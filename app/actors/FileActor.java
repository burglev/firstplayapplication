package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import play.mvc.Http;

import java.io.File;

import static play.mvc.Results.ok;

public class FileActor extends AbstractActor {
    public static Props getProps() {
        return Props.create(FileActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FileActorProtocol.FileUploadMessage.class, message -> {
                    Http.MultipartFormData<File> formData = message.body.asMultipartFormData();
                    Http.MultipartFormData.FilePart<File> filePart = formData.getFile("name");

                    File file = filePart.getFile();

                    File dest = new File(message.fileFolder + "/" + file.getName());
                    file.renameTo(dest);

                    sender().tell(ok("File processing started..."), self());
                })
                .matchAny(unknown -> System.out.println("FileActor.createReceive(): Received an unknown message."))
                .build();

    }
}
