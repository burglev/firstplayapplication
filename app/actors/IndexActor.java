package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import models.Person;

import java.util.List;

public class IndexActor extends AbstractActor {

    public static Props getProps() {
        return Props.create(IndexActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(HelloActorProtocol.PersonList.class, applyHello -> {               //Az applyHello egy HelloActorProtocol objektum: name, ms
                    List<Person> persons = Person.db().find(Person.class).findList();
                    sender().tell(persons, self());
                })
                .build();
    }
}
