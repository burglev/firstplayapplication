package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.fasterxml.jackson.databind.JsonNode;
import models.Person;
import play.libs.Json;
import views.html.index;
import play.mvc.*;

import java.util.List;

import static play.mvc.Results.ok;

public class PersonActor extends AbstractActor {
    public static Props getProps() {
        return Props.create(PersonActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(PersonActorProtocol.DeletePersonMessage.class, message -> {
                    String retVal;
                    if (Person.db().delete(Person.class, message.id) > 0) {
                        retVal = message.id + ". person deleted.";
                    } else {
                        retVal = message.id + ". person not found.";
                    }
                    Result res = ok(retVal);
                    sender().tell(res, self());
                })
                .match(PersonActorProtocol.PersonListRenderMessage.class, message -> {
                    List<Person> persons = Person.db().find(Person.class).findList();
                    Result res = ok(index.render("Play application is ready.", persons));
                    sender().tell(res, self());
                })
                .match(PersonActorProtocol.PersonListMessage.class, message -> {
                    List<Person> persons = Person.db().find(Person.class).findList();
                    JsonNode retVal = Json.toJson(persons);
                    Result res = ok(retVal).as("application/json");
                    sender().tell(res, self());
                })
                .match(PersonActorProtocol.PersonCreateMessage.class, message -> {
                    JsonNode body = message.body;
                    String retVal;
                    if(body.has("name") && body.has("birthdate")) {
                        Person p = new Person();
                        p.setName(body.get("name").asText());
                        p.setBirthdate(body.get("birthdate").asText());
                        p.save();
                        retVal = "Person added: "+body.get("name")+", "+body.get("birthdate");
                    } else {
                        retVal = "Invalid person.";
                    }
                    Result res = ok(retVal);
                    sender().tell(res, self());
                })
                .match(PersonActorProtocol.PersonDetailMessage.class, message -> {
                    Person p = Person.db().find(Person.class, message.id);
                    JsonNode retVal = Json.toJson(p);
                    Result res = ok(retVal).as("application/json");
                    sender().tell(res, self());
                })
                .match(PersonActorProtocol.PersonModifyMessage.class, message -> {
                    JsonNode body = message.body;
                    Person p = Person.db().find(Person.class, message.id);
                    // if (p == null) {}                    }
                    if (body.has("name")) {
                        p.setName(body.get("name").asText());
                    }
                    if (body.has("birthdate")) {
                        p.setBirthdate(body.get("birthdate").asText());
                    }
                    p.update();
                    sender().tell(ok("Person data modified"), self());
                })
                .build();
    }
}

