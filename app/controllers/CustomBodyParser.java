package controllers;

import akka.stream.IOResult;
import akka.stream.Materializer;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import play.api.http.HttpErrorHandler;
import play.core.parsers.Multipart;
import play.libs.streams.Accumulator;
import play.mvc.BodyParser;
import play.mvc.Http;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

class CustomBodyParser extends BodyParser.DelegatingMultipartFormDataBodyParser<File> {

    @Inject
    public CustomBodyParser(Materializer materializer, play.api.http.HttpConfiguration config, HttpErrorHandler errorHandler) {
        super(materializer, config.parser().maxDiskBuffer(), errorHandler);
    }

    @Override
    public Function<Multipart.FileInfo, Accumulator<ByteString, Http.MultipartFormData.FilePart<File>>> createFilePartHandler() {
        return this::apply;
    }

    private File tempGenerator() {
        try {
            final Path path = Files.createTempFile("multipartBody", "tempFile");
            return path.toFile();
        } catch (IOException exc) {
            throw new IllegalStateException(exc);
        }
    }

    private Accumulator<ByteString, Http.MultipartFormData.FilePart<File>> apply(Multipart.FileInfo fileInfo) {
        final String fileName = fileInfo.fileName();
        final String partName = fileInfo.partName();
        System.out.println("Content type: " + fileInfo.contentType());
        final String contetntType = fileInfo.contentType().getOrElse(null);
        final File file = tempGenerator();

        final Sink<ByteString, CompletionStage<IOResult>> sink = FileIO.toFile(file);

        return Accumulator.fromSink(
                sink.mapMaterializedValue(
                        completionStage -> completionStage.thenApplyAsync(
                                result -> new Http.MultipartFormData.FilePart<>(partName, fileName, contetntType, file)
                        )
                )
        );
    }

}
