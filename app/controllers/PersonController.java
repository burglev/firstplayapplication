package controllers;

import actors.PersonActor;
import actors.PersonActorProtocol;
import actors.RouterActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static akka.pattern.Patterns.ask;

public class PersonController extends Controller {
    private final ActorRef routerActorRef;
    private ActorSystem actSys;
    private final long timeoutMilis;

    @Inject
    public PersonController(ActorSystem actSys) {
        this.actSys = actSys;
        routerActorRef = actSys.actorOf(RouterActor.getProps());
        timeoutMilis = 10000;
    }

    /*
    * Egyből visszaadjuk az eredményt
    **/
    public CompletionStage<Result> listPersons() {
        return FutureConverters
                .toJava(ask(actSys.actorOf(PersonActor.getProps()), new PersonActorProtocol.PersonListMessage(), timeoutMilis))
                .thenApply(result -> (Result) result);
    }

    public CompletionStage<Result> personDetail(Long id) {
        return FutureConverters
                .toJava(ask(actSys.actorOf(PersonActor.getProps()), new PersonActorProtocol.PersonDetailMessage(id), timeoutMilis))
                .thenApply(result -> (Result) result);
    }

    /*
    * Válaszolunk,h elkezdtük a műveletet s közben megcsináljuk a háttérben...
    **/
    public CompletionStage<Result> addPerson() {
        JsonNode body = request().body().asJson();
        return FutureConverters
                .toJava(ask(routerActorRef, new PersonActorProtocol.PersonCreateMessage(body), timeoutMilis))
                .thenApply(result -> (Result) result);
    }

    public CompletionStage<Result> modifyPerson(Long id) {
        JsonNode body = request().body().asJson();
        return FutureConverters
                .toJava(ask(routerActorRef, new PersonActorProtocol.PersonModifyMessage(id, body), timeoutMilis))
                .thenApply(result-> (Result) result);
    }

    public CompletionStage<Result> deletePerson(Long id) {
        return FutureConverters
                .toJava(ask(routerActorRef, new PersonActorProtocol.DeletePersonMessage(id), timeoutMilis))
                .thenApply(result -> (Result) result);
    }
}
