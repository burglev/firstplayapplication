package controllers;

import actors.HelloActorBYZ;
import actors.HelloActorProtocol;
import actors.PersonActor;
import actors.PersonActorProtocol;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.CompletionStage;

import static akka.pattern.Patterns.ask;

@Singleton
public class HomeController extends Controller {

    private final ActorRef personActor;
    private final ActorSystem actSys;

    @Inject
    public HomeController(ActorSystem actSys){
        personActor = actSys.actorOf(PersonActor.getProps());
        this.actSys = actSys;
    }

    public CompletionStage<Result> index() {
        return FutureConverters
                .toJava(ask(personActor, new PersonActorProtocol.PersonListRenderMessage(), 10000))
                .thenApply(response -> (Result) response);
    }

    public Result uploadFile() throws IOException {
        final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("name");
        final File file = filePart.getFile();
        final long data = tempFileSize(file);

        return ok("File size: " + data);
    }

    private long tempFileSize(File file) throws IOException {
        final long size = Files.size(file.toPath());
        Files.deleteIfExists(file.toPath());
        return size;
    }

    public CompletionStage<Result> localHello(String name, Long ms) {
        return FutureConverters
                .toJava(
                    ask(
                        actSys.actorOf(Props.create(HelloActorBYZ.class)),
                        new HelloActorProtocol.SayHello(name, ms),
                        5000
                    )
                )
                .thenApply(response -> (Result) response);
    }
}

