package controllers;

import actors.FileActor;
import actors.FileActorProtocol;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import models.Person;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static akka.pattern.Patterns.ask;

public class FileUploadController extends Controller {

    private ActorRef fileActorRef;
    ActorSystem actSys;
    private Config config;
    private final long timeout;

    @Inject
    public FileUploadController(ActorSystem actSys, Config config) {
//        fileActorRef = actSys.actorOf(FileActor.getProps());
        this.actSys = actSys;
        this.config = config;
        timeout = 100000;
    }

    @BodyParser.Of(CustomBodyParser.class)
    public CompletionStage<Result> fileUpload() {
        Http.RequestBody body = request().body();
        String fileFolder = config.getString("myUploadPath");

        return FutureConverters
                .toJava(ask(actSys.actorOf(FileActor.getProps()), new FileActorProtocol.FileUploadMessage(body, fileFolder), timeout))
                .thenApply(result -> (Result)result);
    }

//    private ObjectMapper mapper;
//
//    public FileUploadController() {
//        this.mapper = new ObjectMapper();
//    }
//
//    private long operateOnTempFile(File file) throws IOException {
//        long size = Files.size(file.toPath());
//        Files.deleteIfExists(file.toPath());
//
//        return size;
//    }
//
//    @BodyParser.Of(CustomBodyParser.class)
//    public Result fileUpload() {
//        final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
//        final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("name");
//        final File file = filePart.getFile();
//
//        try {
//            System.out.println(new String(Files.readAllBytes(file.toPath())));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        long data = -1;
//        try {
//            data = operateOnTempFile(file);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("File size: " + Long.toString(data) + " KB");
//        return ok("File upload successful.");
//    }

    @BodyParser.Of(CustomBodyParser.class)
    public Result jsonFileParser() throws FileNotFoundException {
        final Http.MultipartFormData<File> formData = request().body().asMultipartFormData();
        final Http.MultipartFormData.FilePart<File> filePart = formData.getFile("name");

        final File file = filePart.getFile();

        try {
//            Person p1 = mapper.readValue(file, Person.class);
//            p1.save();
            ObjectMapper mapper = new ObjectMapper();
            List<Person> persons = Arrays.asList(mapper.readValue(file, Person[].class));
            for (Person p : persons) {
                p.save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ok("JSON file parsed");
    }

}
