#!/usr/bin/env bash

echo "[" > "$2"

for (( i=1; i<$1; i++ )); do
    if (( $i%10000 == 0 )); then
        echo "$i"
    fi

    echo "{\"name\":\"Béla_$i\",\"birthdate\":\"1987.01.02\"}," >> "$2"
done

echo "{\"name\":\"Béla_$i\",\"birthdate\":\"1987.01.02\"}" >> "$2"

echo "]" >> "$2"
