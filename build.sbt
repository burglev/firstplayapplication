name := """FirstPlayApplication"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.12.2"

libraryDependencies += guice
// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind/2.4.0
libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.4.0"

/*
libraryDependencies += javaJdbc
libraryDependencies += ehcache
libraryDependencies += "com.typesafe.akka" % "akka-remote_2.10" % "2.2.3"
//libraryDependencies += "com.geekcap.informit.akka" % "akka-messages" % "1.0-SNAPSHOT"
*/

/*
libraryDependencies ++= Seq(
  javaJdbc,
  ehcache,
  "com.typesafe.akka" % "akka-remote_2.10" % "2.2.3"/*,
  "com.geekcap.informit.akka" % "akka-messages" % "1.0-SNAPSHOT"*/
)
*/
