# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table address (
  id                            integer auto_increment not null,
  zipcode                       integer not null,
  city                          varchar(255),
  street                        varchar(255),
  constraint pk_address primary key (id)
);

create table person (
  id                            integer auto_increment not null,
  name                          varchar(255),
  birthdate                     varchar(255),
  constraint pk_person primary key (id)
);


# --- !Downs

drop table if exists address;

drop table if exists person;

